module.exports = async function(context, commands) {
	try {
		 
		 /*var imported = document.createElement('script');
		 imported.src = '/jquery-csv-main/src/jquery.csv.js';
		 document.head.appendChild(imported);
		 music = $.csv.toArrays('user.csv');
		 console.log(music[1][1]);*/
		 //const filfsdsd = await commands.js.run('var array;let reader = new FileReader();let file = new File([array], "./text.txt", {type: "text/plain",}); reader.readAsText(file); reader.onload = () => { return reader.result }');
		 //console.log(filfsdsd);
		 /*const chek = await commands.js.run("const fs = require('fs');const data = fs.readFileSync('user.csv', 'utf8');return data;");
		 console.log(chek);*/
		 /*var array;
		 let reader = new FileReader();
		 let file = new File([array], "user.csv");
		 reader.readAsText(file);
		 reader.onload = () => { console.log(reader.result) }*/
		 /*var array;
		 let file = new File(array, "file:///C:/Users/Vadzim_Vysotski1/Desktop/JmeterScript/Test-plans/juice/Client_Side_sitespeedIO/text.txt");
		 let reader = new FileReader();
		 reader.readAsText(file);
		 console.log(reader.result);*/
		 //var array = await commands.js.run('var rawFile = new XMLHttpRequest();rawFile.open("GET", "file:///C:/Users/Vadzim_Vysotski1/Desktop/JmeterScript/Test-plans/juice/Client_Side_sitespeedIO/user.csv", false);if(rawFile.readyState === 4){if(rawFile.status === 200 || rawFile.status == 0){var allText = rawFile.responseText; return allText;}} else {return "mistake"}');
		 //console.log(array);
		 /*console.log("yes");
		 var rawFile = new XMLHttpRequest();
		 rawFile.open("GET", "file:///C:/Users/Vadzim_Vysotski1/Desktop/JmeterScript/Test-plans/juice/Client_Side_sitespeedIO/user.csv", false);
		 if(rawFile.readyState === 4)
         {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
            }
         }
		 else
		 {
			 return "mistake"
		 }*/
		 
		 //step1
		 await commands.measure.start('Homepage');
		 await commands.navigate('https://juice-shop.herokuapp.com/#/');
		 await commands.measure.stop();
		 await commands.click.byClassName('mat-focus-indicator close-dialog mat-raised-button mat-button-base mat-primary ng-star-inserted');
		 await commands.click.byClassName('cc-btn cc-dismiss');
		 await commands.click.byId('navbarAccount');
		 
		 //step2
		 await commands.measure.start('Login');
		 await commands.click.byIdAndWait('navbarLoginButton');
		 await commands.wait.byXpath("//a[contains(text(), 'Not yet a customer')]", 10000);
		 await commands.measure.stop();
		 
		 //step3
		 await commands.measure.start('Registration');
		 await commands.click.byXpathAndWait("//a[contains(text(), 'Not yet a customer')]");
		 await commands.wait.byXpath("//i[contains(text(), ' person_add ')]", 10000);
		 await commands.measure.stop();
		 
		 var text = "";
		 var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		 for (var i = 0; i < 5; i++) {
			 text += possible.charAt(Math.floor(Math.random() * possible.length));
		 }
		 await commands.addText.byId(text + '@gmail.com','emailControl');
		 await commands.addText.byId('1997035','passwordControl');
		 await commands.addText.byId('1997035','repeatPasswordControl');
		 await commands.click.byId('mat-select-2');
		 await commands.click.byId('mat-option-4');
		 await commands.addText.byId('yes','securityAnswerControl');
		 
		 //step4
		 await commands.measure.start('Login_2');
		 await commands.click.byIdAndWait('registerButton');
		 await commands.wait.byXpath("//a[contains(text(), 'Not yet a customer')]", 10000);
		 await commands.measure.stop();
		 await commands.addText.byId(text + '@gmail.com','email');
		 await commands.addText.byId('1997035','password');
		 
		 //step5
		 await commands.measure.start('Homepage_with_busket');
		 await commands.click.byIdAndWait('loginButton');
		 await commands.wait.byXpath("//span[contains(text(), 'Your Basket')]", 10000);
		 await commands.measure.stop();
		 
		 //step6
		 await commands.measure.start('Basket');
		 await commands.click.byClassNameAndWait('mat-focus-indicator buttons mat-button mat-button-base ng-star-inserted');
		 await commands.wait.byXpath("//h1[contains(text(), 'Your Basket')]", 10000);
		 await commands.measure.stop();
	}
	catch (e){
		await commands.screenshot.take('Problem');
		throw e;
	}
}