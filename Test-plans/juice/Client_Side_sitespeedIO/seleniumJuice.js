module.exports = async function(context, commands) {
  const webdriver = context.selenium.webdriver;
  const driver = context.selenium.driver;
    try {
		
		
        //step1
		await commands.measure.start('Homepage');
        await driver.get('https://juice-shop.herokuapp.com/');
		await driver.wait(webdriver.until.elementLocated(webdriver.By.xpath("//div[contains(text(), '1 ')]")), 10000);
		await commands.measure.stop();
		
        await driver.findElement(webdriver.By.xpath("//span[contains(text(), 'Dismiss')]")).click();
        await driver.findElement(webdriver.By.xpath("//a[contains(text(),'Me want it')]")).click();
		await driver.findElement(webdriver.By.xpath("//span[contains(text(), 'Account')]")).click();
		
		
		//step2
		await commands.measure.start('Login');
		await driver.findElement(webdriver.By.id("navbarLoginButton")).click();
        await driver.wait(webdriver.until.elementLocated(webdriver.By.xpath("//a[contains(text(), 'Not yet a customer')]")), 10000);
		await commands.measure.stop();
		
		//step3
		await commands.measure.start('Registration');
		await driver.findElement(webdriver.By.xpath("//a[contains(text(), 'Not yet a customer')]")).click();
		await driver.wait(webdriver.until.elementLocated(webdriver.By.xpath("//i[contains(text(), ' person_add ')]")), 10000);
		await commands.measure.stop();
		
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 5; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		await driver.findElement(webdriver.By.id("emailControl")).sendKeys(text + '@gmail.com');
		await driver.findElement(webdriver.By.id("passwordControl")).sendKeys('1997035');
		await driver.findElement(webdriver.By.id("repeatPasswordControl")).sendKeys('1997035');
		await driver.findElement(webdriver.By.id("mat-select-2")).click();
		await driver.findElement(webdriver.By.id("mat-option-4")).click();
		await driver.findElement(webdriver.By.id("securityAnswerControl")).sendKeys('yes');
		await commands.screenshot.take('RegINFO');
		
		//step4
		await commands.measure.start('Login_2');
		await driver.findElement(webdriver.By.id("registerButton")).click();
		await driver.wait(webdriver.until.elementLocated(webdriver.By.xpath("//a[contains(text(), 'Not yet a customer')]")), 10000);
		await commands.measure.stop();
		
		await driver.findElement(webdriver.By.id("email")).sendKeys(text + '@gmail.com');
		await driver.findElement(webdriver.By.id("password")).sendKeys('1997035');
		
		//step5
		await commands.measure.start('Homepage_with_basket');
		await driver.findElement(webdriver.By.id("loginButton")).click();
		await driver.wait(webdriver.until.elementLocated(webdriver.By.xpath("//div[contains(text(), '1 ')]")), 10000);
		await commands.measure.stop();
		
		//step6
		await commands.measure.start('Basket');
		await driver.findElement(webdriver.By.xpath("//span[contains(text(), 'Your Basket')]")).click();
		await driver.wait(webdriver.until.elementLocated(webdriver.By.xpath("//h1[contains(text(), 'Your Basket')]")), 10000);
		await commands.measure.stop();
    }
    catch (e){
		await commands.screenshot.take('Problem');
		throw e;
	}
}