#!/bin/sh
# for jmeter plugins

docker='C:\Program Files\Docker\Docker\resources\bin\docker.exe';
myfilenames='ls ./*.jar'
libs='docker exec JMETER_5 ls /opt/apache-jmeter-5.3/lib'
"$docker" exec JMETER_5 ls /opt/apache-jmeter-5.3/lib
plugins='docker exec JMETER_5 ls /opt/apache-jmeter-5.3/lib/ext'
a="0"
b="0"
for eachfile in $myfilenames
do
    echo $eachfile;
    for lib in $libs
    do
		if [ "$eachfile" = "$lib" ];then
		a="1";
		fi
	done
	for ext in $plugins
	do
		if [ "$eachfile" = "$ext" ];then
		b="1";
		fi
	done
done
if [ "$a" = "0" ];then
su - root -c "docker cp jmeter-plugins-casutg-2.9.jar JMETER_7:/opt/apache-jmeter-5.3/lib/jmeter-plugins-casutg-2.9.jar";
fi;
if [ "$b" = "0" ];then
su - root -c "docker cp jmeter-plugins-cmn-jmeter-0.6.jar JMETER_7:/opt/apache-jmeter-5.3/lib/ext/jmeter-plugins-cmn-jmeter-0.6.jar";
su - root -c "docker cp jmeter-plugins-manager-1.6.jar JMETER_7:/opt/apache-jmeter-5.3/lib/ext/jmeter-plugins-manager-1.6.jar";
fi;