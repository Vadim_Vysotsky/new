/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.97673065735893, "KoPercent": 0.02326934264107039};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.777573734001113, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9935897435897436, 500, 1500, "Step_3-1"], "isController": false}, {"data": [0.9987179487179487, 500, 1500, "Step_4-0"], "isController": false}, {"data": [0.9987179487179487, 500, 1500, "Step_4-1"], "isController": false}, {"data": [1.0, 500, 1500, "Step_5-0"], "isController": false}, {"data": [0.0, 500, 1500, "Homepage"], "isController": false}, {"data": [0.9923076923076923, 500, 1500, "Step_5-1"], "isController": false}, {"data": [1.0, 500, 1500, "Step_2-0"], "isController": false}, {"data": [0.9948717948717949, 500, 1500, "Step_2-1"], "isController": false}, {"data": [0.9961538461538462, 500, 1500, "Step_3-0"], "isController": false}, {"data": [0.7294871794871794, 500, 1500, "Step_2"], "isController": false}, {"data": [0.7442455242966752, 500, 1500, "Start "], "isController": false}, {"data": [0.9987179487179487, 500, 1500, "Step_4_code"], "isController": false}, {"data": [0.5, 500, 1500, "Step_4_Controller"], "isController": true}, {"data": [0.9948849104859335, 500, 1500, "Start -1"], "isController": false}, {"data": [0.7435897435897436, 500, 1500, "Step_3"], "isController": false}, {"data": [0.7128205128205128, 500, 1500, "Step_4"], "isController": false}, {"data": [0.7307692307692307, 500, 1500, "Step_5"], "isController": false}, {"data": [0.9961636828644501, 500, 1500, "Start -0"], "isController": false}, {"data": [0.6415816326530612, 500, 1500, "Homepage-4"], "isController": false}, {"data": [0.9974489795918368, 500, 1500, "Homepage-3"], "isController": false}, {"data": [0.03443877551020408, 500, 1500, "Homepage-2"], "isController": false}, {"data": [0.6020408163265306, 500, 1500, "Homepage-1"], "isController": false}, {"data": [0.4923469387755102, 500, 1500, "Homepage-0"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 8595, 2, 0.02326934264107039, 648.4330424665502, 109, 38429, 307.0, 1336.800000000001, 2611.199999999999, 3699.119999999997, 10.29784062848578, 152.77678610474445, 17.218997917218303], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Step_3-1", 390, 0, 0.0, 265.3820512820513, 207, 2598, 240.5, 308.90000000000003, 312.45, 506.6799999999987, 0.47396014359777067, 0.9475405114394534, 0.5064428387356688], "isController": false}, {"data": ["Step_4-0", 390, 0, 0.0, 256.8692307692309, 204, 682, 237.0, 307.0, 311.45, 371.25999999999965, 0.47396071959390074, 0.5234859119733416, 0.8588176309286106], "isController": false}, {"data": ["Step_4-1", 390, 0, 0.0, 259.8487179487181, 205, 582, 243.5, 309.0, 313.0, 354.599999999999, 0.4740154456109816, 0.9978597234089489, 0.5065019308533008], "isController": false}, {"data": ["Step_5-0", 390, 0, 0.0, 253.46666666666678, 202, 402, 238.0, 307.0, 310.45, 360.09, 0.47411571341042374, 0.5218050869272925, 0.6865655209680714], "isController": false}, {"data": ["Homepage", 392, 1, 0.25510204081632654, 3424.7091836734703, 2241, 38429, 3317.0, 3849.6, 3991.35, 7082.619999999959, 0.46966300481284773, 68.26164632458746, 2.273842043315629], "isController": false}, {"data": ["Step_5-1", 390, 0, 0.0, 267.9641025641027, 204, 2002, 244.0, 309.0, 315.45, 636.0899999999924, 0.4741289703742336, 0.9075124823569315, 0.505697202851825], "isController": false}, {"data": ["Step_2-0", 390, 0, 0.0, 250.6051282051282, 203, 372, 232.0, 307.90000000000003, 314.0, 359.09, 0.4737051149220755, 0.523203598610222, 0.6771814841727833], "isController": false}, {"data": ["Step_2-1", 390, 0, 0.0, 264.90512820512816, 210, 1037, 244.0, 310.0, 321.9, 515.27, 0.473751724942178, 1.4005724291224904, 0.5062201361793259], "isController": false}, {"data": ["Step_3-0", 390, 0, 0.0, 253.02820512820497, 203, 749, 229.5, 306.90000000000003, 315.0, 509.2099999999958, 0.47392904188560564, 0.5234509241920118, 0.7492389033768052], "isController": false}, {"data": ["Step_2", 390, 0, 0.0, 515.8076923076915, 414, 1266, 505.5, 613.9000000000001, 622.0, 761.4299999999969, 0.47357569075264533, 1.9231126623666397, 1.1830285043993967], "isController": false}, {"data": ["Start ", 391, 0, 0.0, 521.5626598465475, 407, 2650, 504.0, 612.8, 621.9999999999999, 1110.279999999996, 0.4746879624717281, 1.6460083473301534, 1.1756098196216094], "isController": false}, {"data": ["Step_4_code", 390, 0, 0.0, 259.57179487179496, 202, 723, 239.0, 309.0, 316.45, 393.5299999999996, 0.474068455484972, 0.49953111666824684, 0.5056326587886214], "isController": false}, {"data": ["Step_4_Controller", 390, 0, 0.0, 776.6435897435899, 613, 1284, 768.0, 916.0, 923.45, 1026.2599999999998, 0.47370856720163346, 2.0195730173929993, 1.869783559594967], "isController": true}, {"data": ["Start -1", 391, 0, 0.0, 264.4373401534526, 206, 2343, 237.0, 310.0, 322.0, 428.119999999997, 0.47486610233303783, 1.1221401576106098, 0.5074118601907966], "isController": false}, {"data": ["Step_3", 390, 0, 0.0, 518.6974358974362, 414, 2807, 502.5, 611.9000000000001, 616.45, 963.1599999999994, 0.4738023733854273, 1.4705361172563682, 1.2553129077433887], "isController": false}, {"data": ["Step_4", 390, 0, 0.0, 517.0717948717951, 411, 977, 508.0, 611.0, 615.0, 717.3599999999999, 0.4738340341622189, 1.5208238193179706, 1.3648961631440955], "isController": false}, {"data": ["Step_5", 390, 0, 0.0, 521.7666666666669, 406, 2308, 506.5, 612.9000000000001, 616.45, 928.6299999999949, 0.4739491999372929, 1.4287902150453349, 1.191829856338706], "isController": false}, {"data": ["Start -0", 391, 0, 0.0, 256.7723785166241, 201, 1059, 232.0, 306.0, 310.0, 426.99999999999443, 0.4748159329283028, 0.5244304884198344, 0.66856849827014], "isController": false}, {"data": ["Homepage-4", 392, 0, 0.0, 783.5765306122449, 204, 5628, 919.0, 1108.4, 1132.05, 2183.2099999999973, 0.4713266281509448, 3.367868103691858, 0.5280255983263095], "isController": false}, {"data": ["Homepage-3", 392, 0, 0.0, 161.33418367346954, 109, 1140, 150.5, 193.0, 204.0, 324.8999999999995, 0.4719002653235165, 0.6919666973993721, 0.2050738457704735], "isController": false}, {"data": ["Homepage-2", 392, 0, 0.0, 2286.98724489796, 1411, 14332, 2305.5, 2762.4, 2871.2499999999995, 5115.659999999999, 0.47086464923586596, 49.56770090725528, 0.5252089011436486], "isController": false}, {"data": ["Homepage-1", 392, 1, 0.25510204081632654, 1108.2244897959185, 409, 37385, 1143.0, 1363.8, 1439.7499999999998, 2701.8299999999995, 0.4711510147174596, 13.340262425180738, 0.5246456601823306], "isController": false}, {"data": ["Homepage-0", 392, 0, 0.0, 1030.1020408163265, 824, 5526, 1012.5, 1145.7, 1224.35, 2019.7999999999997, 0.47076003727650906, 1.4810960234887642, 0.4978815610679047], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond", 1, 50.0, 0.011634671320535195], "isController": false}, {"data": ["Assertion failed", 1, 50.0, 0.011634671320535195], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 8595, 2, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond", 1, "Assertion failed", 1, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Homepage", 392, 1, "Assertion failed", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Homepage-1", 392, 1, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
