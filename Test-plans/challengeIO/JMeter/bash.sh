#!/bin/bash
# for jmeter plugins
myfilenames='ls ./*.jar'
libs='ls ./newJmeter/lib/*.jar'
plugins='ls ./newJmeter/lib/ext/*.jar'
a="0"
b="0"
for eachfile in $myfilenames
do
    echo $eachfile;
    for lib in $libs
    do
		if [ "$eachfile" = "$lib" ];then
		a="1";
		fi
	done
	for ext in $plugins
	do
		if [ "$eachfile" = "$ext" ];then
		b="1";
		fi
	done
done
if [ "$a" = "0" ];then
find . -name 'jmeter-plugins-casutg-2.9.jar' -exec cp {} ./newJmeter/lib/ \;
fi;
if [ "$b" = "0" ];then
find . -name 'jmeter-plugins-cmn-jmeter-0.6.jar' -exec cp {} ./newJmeter/lib/ext/ \;
find . -name 'jmeter-plugins-manager-1.6.jar' -exec cp {} ./newJmeter/lib/ext/ \;
fi;