module.exports = async function(context, commands) {
	try {
		
		//Homepage
		await commands.measure.start('Homepage');
		await commands.navigate('https://challenge.flood.io/');
		await commands.measure.stop();
		
		//step2
		await commands.measure.start('Step2');
		await commands.click.byClassNameAndWait('btn btn-xl btn-default');
		await commands.wait.byXpath("//hr[contains(text(), '')]", 10000);
		await commands.measure.stop();
		await commands.set.valueById('20','challenger_age');
		
		
		//step3
		await commands.measure.start('Step3');
		await commands.click.byClassNameAndWait('btn');
		await commands.wait.byXpath("//table[contains(text(), '')]",10000);
		await commands.measure.stop();
		
		const firstElement = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[0].innerText)'); 
		const secondElement = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[1].innerText)'); 
		const thirdElement = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[2].innerText)'); 
		const forthElement = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[3].innerText)'); 
		const fifthElement = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[4].innerText)'); 
		 
		const forFirst = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[0].htmlFor)');
		const forSecond = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[1].htmlFor)');
		const forThird = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[2].htmlFor)');
		const forForth = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[3].htmlFor)');
		const forFifth = await commands.js.run('return(document.getElementsByClassName("collection_radio_buttons")[4].htmlFor)');
		 
		var first = parseInt(firstElement);
		var second = parseInt(secondElement);
		var third = parseInt(thirdElement);
		var forth = parseInt(forthElement);
		var fifth = parseInt(fifthElement);
		const Max = Math.max(first, second, third, forth, fifth);
		if(Max === first)
		{
		 await commands.click.byId(forFirst);
		 await commands.addText.byId(firstElement, 'challenger_largest_order');
		}
		else if(Max === second)
		{
		  await commands.click.byId(forSecond);
		  await commands.addText.byId(secondElement, 'challenger_largest_order');
		}
		else if(Max === third)
		{
		  await commands.click.byId(forThird);
		  await commands.addText.byId(thirdElement, 'challenger_largest_order');
		}
		else if(Max === forth)
		{
		  await commands.click.byId(forForth);
		  await commands.addText.byId(forthElement, 'challenger_largest_order');
		}
		else
		{
		  await commands.click.byId(forFifth);
		  await commands.addText.byId(fifthElement, 'challenger_largest_order');
		}
		
		
		//step4
		await commands.measure.start('Step4');
		await commands.click.byClassNameAndWait('btn');
		await commands.wait.byXpath("//h4[contains(text(), 'This step is easy!')]", 10000);
		await commands.measure.stop();
		
		
		//step5
		await commands.measure.start('Step5');
		await commands.click.byClassNameAndWait('btn');
		await commands.wait.byXpath("//span[contains(text(), 'One Time Token')]", 10000);
		await commands.measure.stop();
		const timeToken = await commands.js.run('return(document.getElementsByClassName("token")[0].innerText)');
		await commands.addText.byId(timeToken, 'challenger_one_time_token');
		
		
		//step6
		await commands.measure.start('Done');
		await commands.click.byClassNameAndWait('btn');
		await commands.wait.byXpath("//a[contains(text(), 'Start Again')]", 10000);
		await commands.measure.stop();
	}
	catch (e){
		await commands.screenshot.take('Problem');
		throw e;
	}
}